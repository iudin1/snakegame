// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKESGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* MeshComponents;

	UPROPERTY(BlueprintReadWrite)
		AFood* Food;
		
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> SnakeFood;
	


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetFoodMaterial();



	virtual void Interact(AActor* Interactable, bool bIsHead) override;

	//���������� ��� �������� ���
	// 
	//���������� �� ��� �
	float MinX = -1150.f;
	float MaxX = 1150.f;
	//���������� �� ��� �
	float MinY = -2900.f;
	float MaxY = 2900.f;
	//���������� �� ��� Z
	float SpawnZ = 50.f;
};
