// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BonusDebuff.generated.h"


UCLASS()
class SNAKESGAME_API ABonusDebuff : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusDebuff();
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponentss;
	UPROPERTY(BlueprintReadWrite)
		ABonusDebuff* DebufF;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonusDebuff> Debuffood;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Debuff(AActor* Interactable, bool bIsHead)override;
};
