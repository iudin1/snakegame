// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"

// Add default functionality here for any IInteractable functions that are not pure virtual.

void IInteractable::Interact(AActor* interact,bool bIsHead)
{

}

void IInteractable::SpeedUp(AActor* interact, bool bIsHead)
{

}

void IInteractable::Debuff(AActor* interact, bool bIsHead)
{
}

void IInteractable::Death(AActor* interact, bool bIsHead)
{
}

