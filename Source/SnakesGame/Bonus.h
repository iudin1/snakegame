// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.h"
#include "Bonus.generated.h"


UCLASS()
class SNAKESGAME_API ABonus : public AActor,public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponentsss;
	UPROPERTY(BlueprintReadWrite)
		ABonus* Bonus;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonus> BonusFood;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void SpeedUp(AActor* Interactable, bool bIsHead) override;

};
