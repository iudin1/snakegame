// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Snake.h"
#include "Components/StaticMeshComponent.h"



// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponents = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponents"));
	MeshComponents->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponents->SetCollisionResponseToAllChannels(ECR_Overlap);
	RootComponent = MeshComponents;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::SetFoodMaterial()
{
	//MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AFood::HandleBeginOverlap);
}




void AFood::Interact(AActor* Interactable,bool bIsHead)
{
	
	if (bIsHead)
	{
		auto Snake = Cast <ASnake>(Interactable);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			//�������� ���
			FRotator StartPointRotation = FRotator(0, 0, 0);
			//�������� ��� �� �����������
			float SpawnX = FMath::FRandRange(MinX, MaxX);
			float SpawnY = FMath::FRandRange(MinY, MaxY);
			FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
			if (GetWorld())
			{
				
				Food = GetWorld()->SpawnActor<AFood>(SnakeFood, StartPoint, StartPointRotation);
				
			}
			
		
		}
	}
}
