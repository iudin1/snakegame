// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "Snake.h"
#include "Interactable.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"

// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponentsss = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponentsss"));
	MeshComponentsss->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponentsss->SetCollisionResponseToAllChannels(ECR_Overlap);
	RootComponent = MeshComponentsss;
}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

void ABonus::SpeedUp(AActor* Interactable, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast <ASnake>(Interactable);
		if (IsValid(Snake))
		{
			
			
			Snake->SetActorTickInterval(Snake->MovementSpeed-0.3);
			
		}
	}
}

