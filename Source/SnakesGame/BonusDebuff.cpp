// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusDebuff.h"
#include "Snake.h"

// Sets default values
ABonusDebuff::ABonusDebuff()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponentss = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponentss"));
	MeshComponentss->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponentss->SetCollisionResponseToAllChannels(ECR_Overlap);
	RootComponent = MeshComponentss;
}

// Called when the game starts or when spawned
void ABonusDebuff::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusDebuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusDebuff::Debuff(AActor* Interactable, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast <ASnake>(Interactable);
		if (IsValid(Snake))
		{

			
			Snake->SetActorTickInterval(Snake->MovementSpeed + 0.3);

		}
		
	}
	FTimerHandle Handle;
	GetWorldTimerManager().SetTimer(Handle, [this] {}, 3.0f, false);
}

