// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakesGame/BonusDebuff.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusDebuff() {}
// Cross Module References
	SNAKESGAME_API UClass* Z_Construct_UClass_ABonusDebuff_NoRegister();
	SNAKESGAME_API UClass* Z_Construct_UClass_ABonusDebuff();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakesGame();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKESGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ABonusDebuff::StaticRegisterNativesABonusDebuff()
	{
	}
	UClass* Z_Construct_UClass_ABonusDebuff_NoRegister()
	{
		return ABonusDebuff::StaticClass();
	}
	struct Z_Construct_UClass_ABonusDebuff_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponentss_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponentss;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebufF_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DebufF;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Debuffood_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Debuffood;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusDebuff_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakesGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusDebuff_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BonusDebuff.h" },
		{ "ModuleRelativePath", "BonusDebuff.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusDebuff_Statics::NewProp_MeshComponentss_MetaData[] = {
		{ "Category", "BonusDebuff" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "BonusDebuff.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABonusDebuff_Statics::NewProp_MeshComponentss = { "MeshComponentss", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonusDebuff, MeshComponentss), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABonusDebuff_Statics::NewProp_MeshComponentss_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusDebuff_Statics::NewProp_MeshComponentss_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusDebuff_Statics::NewProp_DebufF_MetaData[] = {
		{ "Category", "BonusDebuff" },
		{ "ModuleRelativePath", "BonusDebuff.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABonusDebuff_Statics::NewProp_DebufF = { "DebufF", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonusDebuff, DebufF), Z_Construct_UClass_ABonusDebuff_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABonusDebuff_Statics::NewProp_DebufF_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusDebuff_Statics::NewProp_DebufF_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusDebuff_Statics::NewProp_Debuffood_MetaData[] = {
		{ "Category", "BonusDebuff" },
		{ "ModuleRelativePath", "BonusDebuff.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ABonusDebuff_Statics::NewProp_Debuffood = { "Debuffood", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonusDebuff, Debuffood), Z_Construct_UClass_ABonusDebuff_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ABonusDebuff_Statics::NewProp_Debuffood_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusDebuff_Statics::NewProp_Debuffood_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABonusDebuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonusDebuff_Statics::NewProp_MeshComponentss,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonusDebuff_Statics::NewProp_DebufF,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonusDebuff_Statics::NewProp_Debuffood,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABonusDebuff_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABonusDebuff, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusDebuff_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusDebuff>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABonusDebuff_Statics::ClassParams = {
		&ABonusDebuff::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ABonusDebuff_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ABonusDebuff_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusDebuff_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusDebuff_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusDebuff()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABonusDebuff_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABonusDebuff, 2032407954);
	template<> SNAKESGAME_API UClass* StaticClass<ABonusDebuff>()
	{
		return ABonusDebuff::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABonusDebuff(Z_Construct_UClass_ABonusDebuff, &ABonusDebuff::StaticClass, TEXT("/Script/SnakesGame"), TEXT("ABonusDebuff"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusDebuff);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
