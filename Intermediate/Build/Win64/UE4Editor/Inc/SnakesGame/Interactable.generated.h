// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKESGAME_Interactable_generated_h
#error "Interactable.generated.h already included, missing '#pragma once' in Interactable.h"
#endif
#define SNAKESGAME_Interactable_generated_h

#define SnakesGame_Source_SnakesGame_Interactable_h_13_SPARSE_DATA
#define SnakesGame_Source_SnakesGame_Interactable_h_13_RPC_WRAPPERS
#define SnakesGame_Source_SnakesGame_Interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakesGame_Source_SnakesGame_Interactable_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKESGAME_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKESGAME_API, UInteractable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKESGAME_API UInteractable(UInteractable&&); \
	SNAKESGAME_API UInteractable(const UInteractable&); \
public:


#define SnakesGame_Source_SnakesGame_Interactable_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKESGAME_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKESGAME_API UInteractable(UInteractable&&); \
	SNAKESGAME_API UInteractable(const UInteractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKESGAME_API, UInteractable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable)


#define SnakesGame_Source_SnakesGame_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteractable(); \
	friend struct Z_Construct_UClass_UInteractable_Statics; \
public: \
	DECLARE_CLASS(UInteractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/SnakesGame"), SNAKESGAME_API) \
	DECLARE_SERIALIZER(UInteractable)


#define SnakesGame_Source_SnakesGame_Interactable_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	SnakesGame_Source_SnakesGame_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
	SnakesGame_Source_SnakesGame_Interactable_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakesGame_Source_SnakesGame_Interactable_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	SnakesGame_Source_SnakesGame_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
	SnakesGame_Source_SnakesGame_Interactable_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakesGame_Source_SnakesGame_Interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define SnakesGame_Source_SnakesGame_Interactable_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define SnakesGame_Source_SnakesGame_Interactable_h_10_PROLOG
#define SnakesGame_Source_SnakesGame_Interactable_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakesGame_Source_SnakesGame_Interactable_h_13_SPARSE_DATA \
	SnakesGame_Source_SnakesGame_Interactable_h_13_RPC_WRAPPERS \
	SnakesGame_Source_SnakesGame_Interactable_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakesGame_Source_SnakesGame_Interactable_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakesGame_Source_SnakesGame_Interactable_h_13_SPARSE_DATA \
	SnakesGame_Source_SnakesGame_Interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakesGame_Source_SnakesGame_Interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKESGAME_API UClass* StaticClass<class UInteractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakesGame_Source_SnakesGame_Interactable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
