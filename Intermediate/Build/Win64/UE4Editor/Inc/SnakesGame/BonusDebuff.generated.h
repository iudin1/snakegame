// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKESGAME_BonusDebuff_generated_h
#error "BonusDebuff.generated.h already included, missing '#pragma once' in BonusDebuff.h"
#endif
#define SNAKESGAME_BonusDebuff_generated_h

#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_SPARSE_DATA
#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_RPC_WRAPPERS
#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABonusDebuff(); \
	friend struct Z_Construct_UClass_ABonusDebuff_Statics; \
public: \
	DECLARE_CLASS(ABonusDebuff, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakesGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusDebuff) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusDebuff*>(this); }


#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABonusDebuff(); \
	friend struct Z_Construct_UClass_ABonusDebuff_Statics; \
public: \
	DECLARE_CLASS(ABonusDebuff, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakesGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusDebuff) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusDebuff*>(this); }


#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusDebuff(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABonusDebuff) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusDebuff); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusDebuff); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusDebuff(ABonusDebuff&&); \
	NO_API ABonusDebuff(const ABonusDebuff&); \
public:


#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusDebuff(ABonusDebuff&&); \
	NO_API ABonusDebuff(const ABonusDebuff&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusDebuff); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusDebuff); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABonusDebuff)


#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_PRIVATE_PROPERTY_OFFSET
#define SnakesGame_Source_SnakesGame_BonusDebuff_h_11_PROLOG
#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_SPARSE_DATA \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_RPC_WRAPPERS \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_INCLASS \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakesGame_Source_SnakesGame_BonusDebuff_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_SPARSE_DATA \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_INCLASS_NO_PURE_DECLS \
	SnakesGame_Source_SnakesGame_BonusDebuff_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKESGAME_API UClass* StaticClass<class ABonusDebuff>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakesGame_Source_SnakesGame_BonusDebuff_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
